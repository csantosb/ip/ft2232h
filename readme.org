#+TITLE: Ft2232h

[[https://gitlab.com/ip-vhdl/ft2232h/-/commits/master][https://gitlab.com/ip-vhdl/ft2232h/badges/master/pipeline.svg]]
[[https://gitlab.com/ip-vhdl/ft2232h/-/commits/master][https://gitlab.com/ip-vhdl/ft2232h/badges/master/coverage.svg]]

This project implements a vhdl interface with the [[https://www.ftdichip.com/Products/ICs/FT2232H.htm][ft2232h]] from ftdi. It follows
indications of the reference document under the doc directory.

Details about the implementation are given in the project [[https://ip-vhdl.gitlab.io/ft2232h][pages]].

* Table of Contents                                 :TOC:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[#contents][Contents]]
- [[#use][Use]]
- [[#simulate][Simulate]]
  - [[#cocotb][Cocotb]]
- [[#implement][Implement]]
  - [[#vivado-manually][Vivado (manually)]]
  - [[#vivado-provided-files][Vivado (provided files)]]
  - [[#vivado-fusesoc][Vivado (fusesoc)]]
- [[#ci][CI]]
- [[#license][License]]

* Contents

 - =site= :: web pages

   - =site/Doxyfile= :: doxygen file to produce project implementation [[https://ip-vhdl.gitlab.io/ft2232h/doxysite/index.html][pages]]

   - =site/index.org= :: main [[https://ip-vhdl.gitlab.io/ft2232h][pages]] file

 - =ip= :: ancillary ip blocs, to be used in simulation and implementation

   - =ip/p1_counter= :: example peripheral =p1_counter.vhd=

   - =ip/Cati_output_fifo1= :: fifo core, necessary for example peripheral

 - =src= :: rtl source code

   =readme.vhd= :: used solely for documenting with doxygen

   - =src/ip= :: ip source code, along with its package defining custom types

   - =src/impl= :: implementation, providing an example of instantiation and use

 - =sim/runs/ft2232h.cocotb= :: simulation using cocotb

   Includes =ft2232h_tb.py= [[https://cocotb.readthedocs.io/en/latest/introduction.html][cocotb]] testbench file, a [[https://gitlab.com/ip-vhdl/ft2232h/-/blob/master/sim/runs/ft2232h.cocotb/Makefile][Makefile]] to run it and a
   =wave.gtkw= to observe simulation results with help of [[http://gtkwave.sourceforge.net/][gtkwave]]. Additionally, a
   [[https://gitlab.com/ip-vhdl/ft2232h/-/blob/master/sim/runs/ft2232h.cocotb/testme.sh][testme.sh]] script file is provided too to ease running tests.

   - =hdl= :: wrapper hdl file =ft2232h_tb.vhd= necessary for type conversion

 - =prj_vivado19.2= :: xilinx vivado related files

   Includes =prj_vivado19.2.xpr= project file, as well as a =run_all.sh= and =run_all.tcl=
   build files.

 - =cfg= :: configuration files

   =top.xdc= ::

 - =pics= :: images

Additional files:

=.gitlab-ci.yml= :: [[https://docs.gitlab.com/ee/ci/][gitlab ci]] file

=ft2232h.core= :: [[https://fusesoc.readthedocs.io/en/master/index.html][fusesoc]] core file

=setup.sh= :: configure [[https://www.gnu.org/software/global/][gnu global]] gtags related files

=todo.org= :: left to do

=vhdltool-config.yaml= :: project setup for use with [[https://www.vhdltool.com/][vhdltool]]

* Use

* Simulate

** Cocotb

Refer to [[https://gitlab.com/ip-vhdl/ft2232h/-/tree/master/sim/runs/ft2232h.cocotb][simulation]] directory for details.

[[pics/gtkwave.small.png]]

* Implement

** Vivado (manually)

Create a project. Include these filesets

#+begin_src sh
  [rtl, rtl.implement, periph, periph.implement, constraints.implement]
#+end_src

as defined in =ft2232h.core= file. Then, proceed as usual.

** Vivado (provided files)

Just run

#+begin_src sh
  cd prj_vivado19.2
  ./run_all.sh
#+end_src

** Vivado (fusesoc)

#+begin_src sh
  fusesoc --cores-root $ThisProjectRootDir run --no-export --target=implement ft2232h
#+end_src

* CI

Continuous integration is available with help of the file [[https://gitlab.com/ip-vhdl/ft2232h/-/blob/master/.gitlab-ci.yml][.gitlab-ci.yml]], using the
[[https://hub.docker.com/repository/docker/csantosb/arch-vhdl][csantosb/arch-vhdl]] docker image. Upon each [[https://gitlab.com/ip-vhdl/ft2232h/-/commits/master][commit]] to this repository, a new
[[https://gitlab.com/ip-vhdl/ft2232h/pipelines][pipeline]] is triggered. Within the pipeline, several tests are defined to verify
correctness of the design.

Finally, if all previous tests succeed, a later job builds this project’s [[https://ip-vhdl.gitlab.io/ft2232h][pages]]
with help of [[https://hub.docker.com/repository/docker/csantosb/alpine-doxygen][csantosb/alpine-doxygen]] image. More details may be found [[https://csantosb-blogs.gitlab.io/blog-tech/posts/continuous-integration/index.html][here]].

* License

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the [[http://www.gnu.org/licenses/gpl.txt][GNU General Public License]]
along with this program. If not, see http://www.gnu.org/licenses/gpl.html.
