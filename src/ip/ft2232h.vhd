-- * Usb Interface
--
--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- =Commands= are the full =RegBank= array. They are composed of the =order= (first byte)
-- followed by a variable length of =parameters=.
--
-- ** Intro
--
-- Implements comunication between generic FPGA logic fabric and a PC
-- through a [[*FT2232H][FT2232H]] controller from FTDI
--
-- The logic waits for an interrupt signal in the [[*USB in][USB in]] FSM, updating a
-- =RegBankLength= x 8 bits register bank (the =command=); then, acts accordingly,
-- following the contents of the first word in this bank (the =order=,
-- see [[*Orders][Orders]])
--
-- The [[*USB out][USB out]] FSM implements sending several sources of data back to the PC
--
-- *** Peripherals
--
-- It is possible to connect peripherals to this module. Each
-- peripheral shares a common interface:
--
--  - p1_datain :: in,  output from fifo
--  - p1_empty  :: in,  empty from fifo
--  - p1_rd     :: out, read to fifo
--  - p1_params :: out, parameters
--  - p1_enable :: out, enable signal
--
-- along with the generics
--
--  - p1_data_width   :: datin width, in bytes
--  - p1_params_width :: params register length, in bytes
--
-- It is assumed that each peripheral corresponds to a =command=, made up of an [[*Orders][order]]
-- followed by its =parameters=. The =p1_enable= signal enables the logic to fill-up
-- the peripheral fifo; the =p1_params= register provides a means to configure the
-- order.
--
-- *** Orders
--
-- =Orders= consist of the first byte in the full =RegBank= array, the =command=, which
-- is composed of the =order= (first byte), the command =parameters= and end by a =FF=
-- byte.
--
-- Currently, the following =orders= are implemented
--
--  - X"01" - peripheral 1 :: simple counter
--  - X"06" - peripheral 2 :: asic data
--  - X"CC" - peripheral 3 :: asic slow control
--  - X"07" - peripheral 4 :: ADC data
--  - X"08" - peripheral 5 :: ADC slow control
--  - X"09" - peripheral 6 :: asic S-Curve
--
--  - X"0A" - disable 40MHz clock
--  - X"05" - returns register bank
--  - X"04" - returns up counter
--  - X"0B" - readout control register
--
-- *** Peripheral parameters
--
--  - P2
--
--    + p2_params(23 downto 8) ::
--    + p2_params(7 downto 0) ::
--

-- TODO: Document with doxygen

-- * Libraries

library ieee;

-- pragma translate_off
library osvvm;
use osvvm.AlertLogPkg.all;
use osvvm.TranscriptPkg.all;
-- pragma translate_on

-- * Packages

use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

-- * Entity

-- USB IF Interface
-- Signals are described in table 3.5 of ref. FT2232H

entity ft2232h is
  port (
    main_rst      : in    std_logic;
    usb_if        : inout rc_usb_if;    -- ftdi usb if
    -- Peripherals
    p1_if         : inout rc_peripheral_if_p1;
    p2_if         : inout rc_peripheral_if_p2;
    p3_if         : inout rc_peripheral_if_p3;
    p4_if         : inout rc_peripheral_if_p4;
    p5_if         : inout rc_peripheral_if_p5;
    p6_if         : inout rc_peripheral_if_p6;
    --outs
    disable_40mhz : out   std_logic;
    global_reset  : out   std_logic
    );
end entity ft2232h;

-- * Architecture

architecture behavioral of ft2232h is

  -- ** Signals

  signal fifo_empty       : std_logic_vector(5 downto 0);
  signal printonce        : boolean                                       := true;
  signal saved_word       : std_logic_vector(127 downto 0)                := (others => '0');
  signal data_taken       : std_logic_vector(127 downto 0)                := (others => '0');
  -- signal memo_fifo_dout             : std_logic_vector(143 downto 0)              := (others => '0');
  signal rst_write        : std_logic                                     := '0';
  signal dina             : std_logic_vector(7 downto 0)                  := (others => '0');
  signal my_regbank       : regbank                                       := (others => X"00");
  signal usb_rd_inner     : std_logic                                     := '0';
  signal interrupt        : std_logic                                     := '0';
  signal txe_one          : std_logic                                     := '0';
  signal control_register : std_logic_vector(7 downto 0)                  := X"A5";
  signal px_enable        : std_logic_vector(nb_peripherals - 1 downto 0) := (others => '0');
  signal addrw            : natural                                       := 0;
  signal addrrd           : natural range 0 to RegBankLength              := 0;
  signal old_addrrb       : natural range 0 to RegBankLength              := 0;
  signal maxadd           : natural range 0 to RegBankLength              := 0;
  signal max_loop         : natural range 0 to RegBankLength              := 0;
  signal px_read          : std_logic                                     := '0';

  -- p_if params
  signal p1_params : rc_params_p1(p1_if.params'range) := (others => (others => '0'));
  signal p2_params : rc_params_p2(p2_if.params'range) := (others => (others => '0'));
  signal p3_params : rc_params_p3(p3_if.params'range) := (others => (others => '0'));
  signal p4_params : rc_params_p4(p4_if.params'range) := (others => (others => '0'));
  signal p5_params : rc_params_p5(p5_if.params'range) := (others => (others => '0'));
  signal p6_params : rc_params_p6(p6_if.params'range) := (others => (others => '0'));

  -- pragma translate off
  -- signal ft2232h_tb_id     : alertlogidtype := GetAlertLogID("ft2232_tb_");
  constant ft2232h_id        : alertlogidtype := GetAlertLogID("ft2232h");
  constant ft2232h_id_usbout : alertlogidtype := GetAlertLogID("*ft2232h-usb-out*", ft2232h_id);
  constant ft2232h_id_usbin  : alertlogidtype := GetAlertLogID("*ft2232h-usb-in*", ft2232h_id);
  -- pragma translate on

  -- State signals in the [[*USB in][USB in]] and [[*USB out][USB out]] FSMs.

  type read_state_type is (read_stand_by, read_state_1, read_state_2, read_wait_state);

  type write_state_type is (
    write_rb_stand_by, write_rb_readout, write_rb_end,
    write_data_wait, write_data_send,
    write_data_temp, write_control_register, write_data_take
    );

  signal read_state  : read_state_type;
  signal write_state : write_state_type;

  -- ** Alias

  -- Identifies the board.
  -- alias BoardID : std_logic_vector(7 downto 0) is p2_if.params(0)(23 downto 16);
  constant boardid : std_logic_vector(7 downto 0) := X"CD";

  -- Identifies events in a channel, as readout by the ASIC, in contrast to the
  --    triggers, which are seen by the ASIC discriminator.
  -- neweventsbychannel

begin

  -- ** Generic

  -- Static signal assignments

  usb_if.usb_siwua <= '1';
  -- usb_reset_fpga <= main_rst;          --'0'
  -- button         <= main_rst;          --'0'
  usb_if.usb_rd    <= usb_rd_inner;
  control_register <= X"04";

  -- Send the interrupt out of the controller. This is useful in non-peripheral
  -- blocks which are not driven by a =pX_enable= signal in order to reset them,
  -- erasing fifos, bringing state machines to known states, etc.

  -- Goes high at startup and when an usb interrupt happens.
  global_reset <= interrupt or main_rst;

  -- ** Usb in fsm
  --
  -- Receives data from PC.
  --
  -- This process waits for an interrupt (usb_rxf=0) and refreshes the
  -- register bank. Implements section 4, figure 4.4, "Read Timing" (upper
  -- half) of reference [[*FT2232H][FT2232H]]; details are given in section 4.4.1.

  u_usbin : process (usb_if.clk_2232) is
  begin

    if (usb_if.clk_2232'event and usb_if.clk_2232 = '1') then
      if (main_rst = '1') then
        usb_rd_inner  <= '1';
        usb_if.usb_oe <= '1';
        interrupt     <= '1';
        addrw         <= 0;
        read_state    <= read_stand_by;
      else

        case read_state is

          when read_stand_by =>
            -- wait for a rxf low level
            usb_rd_inner  <= '1';
            usb_if.usb_oe <= '1';
            interrupt     <= '0';
            addrw         <= 0;
            if (usb_if.usb_rxf = '0') then
              -- pragma translate off
              Print("* New USB Transfert");
              BlankLine(1);
              Print("** USB in");
              BlankLine(1);
              Log(ft2232h_id_usbin, "interrrupt received", DEBUG);
              BlankLine(1);
              -- pragma translate on
              read_state <= read_state_1;
            else
              read_state <= read_stand_by;
            end if;
          when read_state_1 =>
            -- asser oe low to accept transaction
            usb_rd_inner  <= '1';
            usb_if.usb_oe <= '0';
            interrupt     <= '1';
            read_state    <= read_state_2;
          when read_state_2 =>
            interrupt     <= '1';
            usb_if.usb_oe <= '0';
            -- ii. normal incoming data capture
            if (usb_rd_inner = '0' and usb_if.usb_rxf = '0') then
              -- pragma translate off
              -- SetLogEnable(debug, false);
              -- setlogenable(debug, false);
              -- Log("probando", debug);
              Log(ft2232h_id_usbin, "data captured, " & to_string(addrw) & " - data " &
                  to_hstring(usb_if.usb_data), DEBUG);
              -- pragma translate on
              addrw             <= addrw + 1;
              my_regbank(addrw) <= usb_if.usb_data;
              dina              <= usb_if.usb_data;
            end if;
            if (usb_if.usb_rxf = '0') then
              -- i. set rd low for as low as data arrives
              usb_rd_inner <= '0';
              read_state   <= read_state_2;
            else
              -- iii. if transaction interrupts, set rd high, get to wait state
              usb_rd_inner <= '1';
              BlankLine(1);
              read_state   <= read_wait_state;
            end if;
          when read_wait_state =>
            usb_rd_inner  <= '1';
            usb_if.usb_oe <= '1';
            interrupt     <= '1';
            -- MaxAdd       <= '0'&addrw;
            maxadd        <= addrw;
            -- pragma translate off
            Log(ft2232h_id_usbin, "data capture is in interrupt", DEBUG);
            -- just one message is enough
            SetLogEnable(ft2232h_id_usbin, DEBUG, false);
            -- pragma translate on
            -- if last word is 0xFF, transaction is over
            if (dina = X"FF") then
              read_state <= read_stand_by;
              -- pragma translate off
              SetLogEnable(ft2232h_id_usbin, DEBUG, true);
              BlankLine(1);
              Log(ft2232h_id_usbin, "data capture is over", DEBUG);
            -- pragma translate on
            --  if transaction continues, go to get data
            elsif (usb_if.usb_rxf = '0') then
              -- pragma translate off
              SetLogEnable(ft2232h_id_usbin, DEBUG, true);
              BlankLine(1);
              Log(ft2232h_id_usbin, "data capture continues ...", DEBUG);
              BlankLine(1);
              -- pragma translate on
              read_state <= read_state_1;
            -- otherwise, wait here
            else
              read_state <= read_wait_state;
            end if;

        end case;

      end if;
    end if;

  end process u_usbin;

  -- ** Peripherals

  -- This logic deals with the out signals to the peripherals from the [[*Orders][order]].

  -- *** perpherals clock domain
  --
  -- All peripherals opperate in the same clock domain, in both directions

  p1_if.clk <= usb_if.clk_2232;
  p2_if.clk <= usb_if.clk_2232;
  p3_if.clk <= usb_if.clk_2232;
  p4_if.clk <= usb_if.clk_2232;
  p5_if.clk <= usb_if.clk_2232;
  p6_if.clk <= usb_if.clk_2232;

  -- *** peripherals enable

  -- These signals enables the peripherals. When there is an =interrupt=, at
  -- =main=rst=, or when =rst_write= is high the peripherals are disabled.

  px_enable <= "000000" when (main_rst = '1' or interrupt = '1' or rst_write = '1') else
               "000001" when my_regbank(0) = X"01" else
               "000010" when my_regbank(0) = X"06" else
               "000100" when my_regbank(0) = X"CC" else
               "001000" when my_regbank(0) = X"07" else
               "010000" when my_regbank(0) = X"08" else
               "100000" when my_regbank(0) = X"09" else
               "000000";

  p1_if.enable <= px_enable(0);
  p2_if.enable <= px_enable(1) or px_enable(5);
  p3_if.enable <= px_enable(2);
  p4_if.enable <= px_enable(3);
  p5_if.enable <= px_enable(4);
  p6_if.enable <= px_enable(5);

  -- *** peripherals read

  -- This hack helps to handle just one =pX_read= signal in the [[*USB out][USB out]] state
  -- machine, avoiding several nested ifs.

  p1_if.rd <= px_enable(0) and px_read;
  p2_if.rd <= px_enable(1) and px_read;
  p3_if.rd <= px_enable(2) and px_read;
  p4_if.rd <= px_enable(3) and px_read;
  p5_if.rd <= px_enable(4) and px_read;
  p6_if.rd <= px_enable(5) and px_read;

  -- *** peripherals params

  -- *>* px params

  -- Here I create the parameters of each command. They are forced to 0
  -- when their corresponding command is not the current.

  -- The =px_params= array takes =px_params_width= bytes from the =RegBank= array
  -- starting at position 1 (as byte 0 is the command).

  u_p1_if_params : for i in p1_if.params'range generate
    -- Fill in array with contents in reg bank

    u_p1_params : for j in 0 to p1_params_width - 1 generate
      p1_params(i)(p1_params(i)'left - j * 8 downto p1_params(i)'left - j * 8 - 7) <=
        my_regbank(p1_params_width * i + j + 1);
    end generate u_p1_params;

    -- Put array at its right place
    p1_if.params(i) <= p1_params(i) when my_regbank(0) = X"01" else
                       p1_if.params(i);
  end generate u_p1_if_params;

  -- Here, I need to send =p2_params_inner= even when not =my_RegBank(0) = X"06"=.
  -- Otherwise, when computing s-curve the =EnablePhysicalData= is zero, and no
  -- =NewEventByChannel= is available.

  -- p2 parameters

  u_p2_if_params : for i in p2_if.params'range generate
    -- Fill in array with contents in reg bank

    u_p2_params : for j in 0 to p2_params_width - 1 generate
      p2_params(i)(p2_params(i)'left - j * 8 downto p2_params(i)'left - j * 8 - 7) <=
        my_regbank(p2_params_width * i + j + 1);
    end generate u_p2_params;

    -- Put array at its right place
    p2_if.params(i) <= p2_params(i) when my_regbank(0) = X"06" else
                       p2_if.params(i);
  end generate u_p2_if_params;

  -- p3 parameters

  u_p3_if_params : for i in p3_if.params'range generate
    -- Fill in array with contents in reg bank

    u_p3_params : for j in 0 to p3_params_width - 1 generate
      p3_params(i)(p3_params(i)'left - j * 8 downto p3_params(i)'left - j * 8 - 7) <=
        my_regbank(p3_params_width * i + j + 1);
    end generate u_p3_params;

    -- Put array at its right place
    p3_if.params(i) <= p3_params(i) when my_regbank(0) = X"CC" else
                       p3_if.params(i);
  end generate u_p3_if_params;

  u_p4_if_params : for i in p4_if.params'range generate
    -- Fill in array with contents in reg bank

    u_p4_params : for j in 0 to p4_params_width - 1 generate
      p4_params(i)(p4_params(i)'left - j * 8 downto p4_params(i)'left - j * 8 - 7) <=
        my_regbank(p4_params_width * i + j + 1);
    end generate u_p4_params;

    -- Put array at its right place
    p4_if.params(i) <= p4_params(i) when my_regbank(0) = X"07" else
                       p4_if.params(i);
  end generate u_p4_if_params;

  u_p5_if_params : for i in p5_if.params'range generate
    -- Fill in array with contents in reg bank

    u_p5_params : for j in 0 to p5_params_width - 1 generate
      p5_params(i)(p5_params(i)'left - j * 8 downto p5_params(i)'left - j * 8 - 7) <=
        my_regbank(p5_params_width * i + j + 1);
    end generate u_p5_params;

    -- Put array at its right place
    p5_if.params(i) <= p5_params(i) when my_regbank(0) = X"08" else
                       p5_if.params(i);
  end generate u_p5_if_params;

  u_p6_if_params : for i in p6_if.params'range generate
    -- Fill in array with contents in reg bank

    u_p6_params : for j in 0 to p6_params_width - 1 generate
      p6_params(i)(p6_params(i)'left - j * 8 downto p6_params(i)'left - j * 8 - 7) <=
        my_regbank(p6_params_width * i + j + 1);
    end generate u_p6_params;

    -- Put array at its right place
    p6_if.params(i) <= p6_params(i) when my_regbank(0) = X"09" else
                       p6_if.params(i);
  end generate u_p6_if_params;

  -- ** Disable 40MHz

  -- The order =“0A”= disables the 40MHz clock. Here I just set a bit.

  u_disable_40hz : process (usb_if.clk_2232) is
  begin

    if (usb_if.clk_2232'event and usb_if.clk_2232 = '1') then
      if (main_rst = '1') then
        disable_40MHz <= '0';
      elsif (my_regbank(0) = X"0A") then
        disable_40MHz <= my_regbank(1)(0);
      end if;
    end if;

  end process u_disable_40hz;

  -- ** Usb out fsm

  -- Sends data back from the FPGA to the PC.

  -- This process sends data to the pc throught the usb if. Implements
  -- section 4, figure 4.4, "Write Timing" (lower half) of reference
  -- [[*FT2232H][FT2232H]]; additional details are given in section 4.4.2.

  rst_write <= '0' when (my_regbank(0) = X"0B" or my_regbank(0) = X"05" or my_regbank(0) = X"04" or
                         my_regbank(0) = X"01" or my_regbank(0) = X"06" or my_regbank(0) = X"CC" or
                         my_regbank(0) = X"07" or my_regbank(0) = X"08" or my_regbank(0) = X"09") else
               '1';

  -- When this signal is set high, the [[*USB out][USB out]] block is on hold and
  -- =pX_enable= is set to 0, effectively disabling all peripherals. Then, when
  -- a valid order appears, it goes low, releasing the data readout
  -- functionality.

  fifo_empty <= not(p6_if.empty & p5_if.empty & p4_if.empty & p3_if.empty & p2_if.empty & p1_if.empty);

  u_usbout : process (usb_if.clk_2232) is

    variable do_log_once : boolean := true;

  begin

    if (usb_if.clk_2232'event and usb_if.clk_2232 = '1') then

      -- *** Sync Reset

      if (interrupt = '1' or rst_write = '1' or main_rst = '1') then
        usb_if.usb_wr   <= '1';
        addrrd          <= 0;
        usb_if.usb_data <= (others => 'Z');
        txe_one         <= '1';
        px_read         <= '0';
        -- following the order, proceed to next state
        if (my_regbank(0) = X"01" or
            my_regbank(0) = X"06" or
            my_regbank(0) = X"CC" or
            my_regbank(0) = X"07" or
            my_regbank(0) = X"08" or
            my_regbank(0) = X"09") then
          write_state <= write_data_wait;
        -- X"0B" readout control register
        elsif (my_regbank(0) = X"0B") then
          write_state <= write_control_register;
        -- X"05" returns register bank
        -- X"04" returns a ramp
        elsif (my_regbank(0) = X"05" or
               my_regbank(0) = X"04") then
          write_state <= write_rb_stand_by;
        end if;
      else

        case write_state is

          -- *** readout data
          --
          -- Brings data from fifo to usb controller chip

          when write_data_wait =>

            -- pragma translate off
            if (printonce) then
              BlankLine(1);
              Print("** USB out, order " & order_to_string(my_regbank(0)) & " - " &
                    to_hstring(my_regbank(0)));
              printonce <= false;
              BlankLine(1);
            end if;
            Log(ft2232h_id_usbout, "  order " & order_to_string(my_regbank(0)) & " - " &
                to_hstring(my_regbank(0)) & " - " & " requested ", DEBUG);
            if (printonce) then
              BlankLine(1);
            end if;
            SetLogEnable(ft2232h_id_usbout, DEBUG, false);
            -- pragma translate on
            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '1');
            addrrd          <= 0;
            txe_one         <= '1';
            if (p1_if.empty = '0' and usb_if.usb_txe = '0') or
              (p2_if.empty = '0' and usb_if.usb_txe = '0') or
              (p3_if.empty = '0' and usb_if.usb_txe = '0') or
              (p4_if.empty = '0' and usb_if.usb_txe = '0') or
              (p5_if.empty = '0' and usb_if.usb_txe = '0') or
              (p6_if.empty = '0' and usb_if.usb_txe = '0') then
              -- pragma translate off
              SetLogEnable(ft2232h_id_usbout, DEBUG, true);
              Log(ft2232h_id_usbout,
                  "  non empty fifo number detected" &
                  to_string(edacom_onehot_to_integer_neg(fifo_empty)) &
                  "  fifo - empty vector is " &
                  to_string(fifo_empty), DEBUG);
              -- pragma translate on
              px_read     <= '1';
              write_state <= write_data_temp;
            else
              px_read     <= '0';
              write_state <= write_data_wait;
            end if;

          when write_data_temp =>

            px_read     <= '0';
            write_state <= write_data_take;

          when write_data_take =>

            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '1');
            addrrd          <= 0;
            px_read         <= '0';
            txe_one         <= '1';
            if (px_enable(0) = '1') then
              data_taken(data_taken'left downto data_taken'left + 1 - p1_data_width * 8) <= p1_if.data;
              max_loop                                                                   <= p1_data_width;
            elsif (px_enable(1) = '1') then
              data_taken(data_taken'left downto data_taken'left + 1 - (p2_data_width + 2) * 8) <= p2_if.data & boardid;
              max_loop                                                                         <= p2_data_width + 2;
            elsif (px_enable(2) = '1') then
              data_taken(data_taken'left downto data_taken'left + 1 - (p3_data_width + 2) * 8) <= p3_if.data & boardid;
              max_loop                                                                         <= p3_data_width + 2;
            elsif (px_enable(3) = '1') then
              data_taken(data_taken'left downto
                         data_taken'left + 1 - p4_data_width * 8) <=
                p4_if.data(15 downto 0) & p4_if.data(31 downto 16) &
                p4_if.data(47 downto 32) & p4_if.data(63 downto 48) &
                p4_if.data(79 downto 64) & p4_if.data(95 downto 80) &
                p4_if.data(111 downto 96) & p4_if.data(127 downto 112);
              max_loop <= p4_data_width;
            elsif (px_enable(4) = '1') then
              data_taken(data_taken'left downto data_taken'left + 1 - p5_data_width * 8) <= p5_if.data;
              max_loop                                                                   <= p5_data_width;
            else
              data_taken(data_taken'left downto data_taken'left + 1 - p6_data_width * 8) <= p6_if.data;
              max_loop                                                                   <= p6_data_width;
            end if;
            do_log_once := true;
            write_state <= write_data_send;

          when write_data_send =>

            if (do_log_once) then
              Log(ft2232h_id_usbout, "  max loop is " & to_string(max_loop), DEBUG);
              BlankLine(1);
              Log(ft2232h_id_usbout, "  data taken is " & to_hstring(data_taken), DEBUG);
              BlankLine(1);
              do_log_once := false;
            end if;
            px_read <= '0';
            if (addrrd = max_loop and usb_if.usb_txe = '0') then
              -- pragma translate off
              Log(ft2232h_id_usbout, "  done reading out " & to_string(addrrd) & " bytes", DEBUG);
              -- pragma translate on
              usb_if.usb_wr   <= '1';
              txe_one         <= '1';
              usb_if.usb_data <= (others => '1');
              write_state     <= write_data_wait;
            elsif (usb_if.usb_txe = '0') then
              -- pragma translate off
              Log(ft2232h_id_usbout,
                  "  sending byte X'" &
                  to_hstring(data_taken(data_taken'left downto data_taken'left + 1 - 8)) &
                  "' at position " &
                  to_string(addrrd),
                  DEBUG);
              -- pragma translate on
              addrrd          <= addrrd + 1;
              usb_if.usb_wr   <= '0';
              txe_one         <= '1';
              usb_if.usb_data <= data_taken(data_taken'left downto data_taken'left + 1 - 8);
              saved_word      <= data_taken;
              data_taken      <= data_taken(data_taken'left + 1 - 9 downto 0) & X"FF";
              write_state     <= write_data_send;
            else
              if (addrrd > 0 and txe_one = '1') then
                addrrd <= addrrd - 1;
              end if;
              txe_one         <= '0';
              usb_if.usb_wr   <= '1';
              usb_if.usb_data <= (others => '1');
              data_taken      <= saved_word;
              write_state     <= write_data_send;
            end if;

          -- *** readout register bank, rb
          --
          -- sends back the register bank

          when write_rb_stand_by =>

            -- pragma translate off
            BlankLine(1);
            Print("** USB out, order " & order_to_string(my_regbank(0)) & " - " & to_hstring(my_regbank(0)));
            BlankLine(1);
            Log(ft2232h_id_usbout, "  order " & order_to_string(my_regbank(0)) & " - " &
                to_hstring(my_regbank(0)) & " - " & " requested ", DEBUG);
            BlankLine(1);
            -- pragma translate on

            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '0');
            addrrd          <= addrrd;
            if (usb_if.usb_txe = '0') then
              write_state <= write_rb_readout;
            else
              write_state <= write_rb_stand_by;
            end if;

          when write_rb_readout =>

            if (addrrd = maxadd and my_regbank(0) = X"05") then
              -- pragma translate off
              Log(ft2232h_id_usbout, "  done reading out " & to_string(addrrd) & " bytes", DEBUG);
              -- pragma translate on
              usb_if.usb_wr   <= '1';
              usb_if.usb_data <= (others => '0');
              addrrd          <= 0;
              write_state     <= write_rb_end;
            elsif (usb_if.usb_txe = '0') then
              usb_if.usb_wr <= '0';
              if (my_regbank(0) = X"05") then
                -- usb_if.usb_data <= my_RegBank(to_integer(addrrd));
                usb_if.usb_data <= my_regbank(addrrd);
                -- pragma translate off
                Log(ft2232h_id_usbout, "  sending byte " & to_hstring(my_regbank(addrrd)) & " at position " &
                    to_string(addrrd), DEBUG);
              -- pragma translate on
              else
                usb_if.usb_data <= (others => '0');  -- std_logic_vector(addrrd(7 downto 0));
              end if;
              addrrd      <= addrrd + 1;
              old_addrrb  <= addrrd;
              write_state <= write_rb_readout;
            else
              usb_if.usb_wr   <= '1';
              usb_if.usb_data <= (others => '0');
              addrrd          <= old_addrrb;
              write_state     <= write_rb_readout;
            end if;

          when write_rb_end =>

            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '0');
            addrrd          <= 0;
            write_state     <= write_rb_end;

          -- *** readout control register

          when write_control_register =>

            -- pragma translate off
            BlankLine(1);
            Print("** USB out, order " & order_to_string(my_regbank(0)) & " - " & to_hstring(my_regbank(0)));
            BlankLine(1);
            Log(ft2232h_id_usbout, "  order " & order_to_string(my_regbank(0)) & " - " &
                to_hstring(my_regbank(0)) & " - " & " requested ", DEBUG);
            BlankLine(1);
            -- pragma translate on
            usb_if.usb_wr   <= '0';
            usb_if.usb_data <= control_register;
            write_state     <= write_rb_end;

        end case;

      end if;
    end if;

  end process u_usbout;

end architecture behavioral;

-- * References
--
-- ** FT2232H
--
-- The communication is based on the FT245 Style Synchronous FIFO Interface, as
-- detailed in the document [[file:~/Projects/perso/Ip-Vhdl/ft2232h/doc/DS_FT2232H.pdf][DS_FT2232H.pdf]].
