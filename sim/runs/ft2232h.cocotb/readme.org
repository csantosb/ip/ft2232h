#+TITLE: Simulate with cocotb

Simulation instructions are based on that of the [[https://gitlab.com/ip-vhdl/eda-common][eda-common]] repository, used here.

*Contents*

Wrapper =hdl/t2232h_tb.vhd= contains an instantiation of the =ft22232h= ip, along with
an example simple peripheral =p1_counter=. Additionally, it provides type
conversion, so that the test bench may access the design use standard types.

The peripheral just sends an up counter to the controller.

The =ft2232h_tb.py= [[https://cocotb.readthedocs.io/en/latest/introduction.html][cocotb]] testbench file includes a single test =usb_if_test=.
Three sub tests are performed:

 - send command 0x03

 - send random command 0x05, reading back same command from controller

 - send command 0x01, capturing peripheral data

All low level transactions are checked, and all sub tests are verified at
simulation run time.

*Run*

=sim/runs/ft2232h.cocotb= directory includes a [[https://gitlab.com/ip-vhdl/ft2232h/-/blob/master/sim/runs/ft2232h.cocotb/Makefile][Makefile]], so just

#+begin_src sh
  cd sim/runs/ft2232h.cocotb
  make EDA_COMMON_DIR=/tmp/eda-common
#+end_src

to run the [[file:pics/sim.gif][simulation]], where =eda-common= may be obtained as follows

#+begin_src sh
  git clone https://gitlab.com/csantosb/eda-common /tmp/eda-common
#+end_src

More details on this are given [[https://gitlab.com/ip-vhdl/eda-common/-/wikis/Use.configure][here]].

Debugging information may be obtained with setting =DEBUG= variable

#+begin_src sh
  make EDA_COMMON_DIR=/tmp/eda-common DEBUG=1
#+end_src

For advanced examples refer to [[https://gitlab.com/ip-vhdl/ft2232h/-/blob/master/sim/runs/ft2232h.cocotb/testme.sh][testme.sh]] script.

Note that pre-compiled simulation libraries are expected to be setup correctly
too. Refer to [[https://gitlab.com/ip-vhdl/eda-common/-/wikis/Use.configure][Libraries]] section for details.

Finally, simulation will provide the resulting [[file:pics/gtkwave.png][waveform]], even if not necessary
to validate the tests.

[[pics/gtkwave.small.png]]

** Using fusesoc

Use provided =testme.fusesoc.sh= script file

#+begin_src sh
  cd sim/runs/ft2232h.cocotb/tests
  ./test.fusesoc.sh
#+end_src

to produce simulation below

[[file:testme.fusesoc.gif]]
