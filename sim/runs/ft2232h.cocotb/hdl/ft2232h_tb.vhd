-- * Libraries

library ieee;
use ieee.std_logic_1164.all;
use work.ft2232h_instance_pkg.all;
use work.edacom_pkg.all;

library osvvm;
context osvvm.OsvvmContext;

-- * Entity

entity ft2232h_tb is
  generic (
    g_nb_asics       : natural range 1 to 8 := 8;
    g_osvvm_loglevel : string               := "none"
    );
  port (
    -- usb if
    usb_rxf      : in  std_logic;
    usb_txe      : in  std_logic;
    usb_oe       : out std_logic;
    usb_rd       : out std_logic;
    usb_wr       : out std_logic;
    usb_data_in  : in  std_logic_vector(7 downto 0);
    usb_data_out : out std_logic_vector(7 downto 0)
    );
end entity ft2232h_tb;

-- * Architecture

architecture test_tb of ft2232h_tb is

  -- ** Signals

  signal rst           : std_logic;
  signal usb_if        : rc_usb_if;
  signal global_reset  : std_logic;
  signal disable_40mhz : std_logic;

  signal p1_if : rc_peripheral_if_p1(params(0 to 0));
  signal p2_if : rc_peripheral_if_p2(params(0 to g_nb_asics - 1));
  signal p3_if : rc_peripheral_if_p3(params(0 to g_nb_asics - 1));
  signal p4_if : rc_peripheral_if_p4(params(0 to 0));
  signal p5_if : rc_peripheral_if_p5(params(0 to 0));
  signal p6_if : rc_peripheral_if_p6(params(0 to 0));

  signal clk      : std_logic;
  signal clk_2232 : std_logic;

  signal osvvm_do_report   : boolean := false;  -- when to do the coverage report

  signal  ft2232h_tb_id : alertlogidtype :=  GetAlertLogID("ft2232_tb_");

begin

  -- ** Transcripts

  u_transcript : process
  begin
    -- SetTranscriptMirror;
    TranscriptOpen("./transcript.org");  -- Transcript file
    wait;
  end process u_transcript;

  -- ** Logging
  --
  -- Set log levels

  u_alert : process
  begin

    SetAlertLogName("test ft2232h tb");
    edacom_SetLogLevel(g_osvvm_loglevel);

    -- SetLogEnable(ft2232h_tb_id, debug, true);
    -- log(ft2232h_tb_id, "probando", debug);

    wait;

  end process u_alert;

  -- ** Reports

  -- Produce coverage and scoreboard reports

  u_reports : process
  begin

    -- wait until tb enables producing the report
    wait until osvvm_do_report;
    -- SetTranscriptMirror(false);
    BlankLine(1);

    -- Log("DATA Statistics:", ALWAYS);
    -- BlankLine(1);
    -- cp_dataout.writebin;

    -- Log("CHANNEL Statistics:", ALWAYS);
    -- BlankLine(1);
    -- cp_channel.writebin;

    -- BlankLine(1);

    -- Log("Scoreboard: channel/data errors: " & to_string(sb_channel_data.GetErrorCount) &
    --     "  Push counts: " & to_string(sb_channel_data.GetPushCount) &
    --     "  Check couts: " & to_string(sb_channel_data.GetCheckCount), ALWAYS);
    -- BlankLine(1);

    -- Log("Scoreboard: channel errors: " & to_string(sb_channel.GetErrorCount) &
    --     "  Push counts: " & to_string(sb_channel.GetPushCount) &
    --     "  Check couts: " & to_string(sb_channel.GetCheckCount), ALWAYS);
    -- BlankLine(1);

    -- Log("Scoreboard: data errors: " & to_string(sb_data.GetErrorCount) &
    --     "  Push counts: " & to_string(sb_data.GetPushCount) &
    --     "  Check couts: " & to_string(sb_data.GetCheckCount), ALWAYS);
    -- BlankLine(1);

    ReportAlerts;
    TranscriptClose;

  end process u_reports;

  -- ** Logic

  -- Logic to test

  -- *** Clock

  -- System clock

  CreateClock(clk => clk, Period => 8 ns);

  CreateClock(clk => clk_2232, Period => 12 ns);

  -- *** Reset

  -- System reset

  CreateReset(reset       => rst,
              resetactive => '1',
              clk         => clk,
              Period      => 1 us,
              tpd         => 0 ns);

  -- *** Ft2232h ctrl

  usb_if.clk_2232 <= clk_2232;

  usb_if.usb_rxf <= usb_rxf;

  usb_if.usb_txe <= usb_txe;

  usb_oe <= usb_if.usb_oe;

  usb_rd <= usb_if.usb_rd;

  usb_wr <= usb_if.usb_wr;

  usb_data_out <= usb_if.usb_data;

  usb_if.usb_data <= usb_data_in;

  u_ft2232h : entity work.ft2232h
    port map (
      main_rst      => rst,
      usb_if        => usb_if,
      p1_if         => p1_if,
      p2_if         => p2_if,
      p3_if         => p3_if,
      p4_if         => p4_if,
      p5_if         => p5_if,
      p6_if         => p6_if,
      disable_40mhz => disable_40mhz,
      global_reset  => global_reset
      );

  -- *** P1 peripheral

  u_p1_counter : entity work.p1_counter
    port map (
      clk          => clk,
      global_reset => global_reset,
      p_if         => p1_if
      );

end architecture test_tb;
