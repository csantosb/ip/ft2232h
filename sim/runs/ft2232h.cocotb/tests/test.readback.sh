#!/bin/sh

export G_NB_ASICS=8		# always use 8 asics

# run readback test, setting regbanklength first

# i.
# export C_REGBANKLENGTH=600	# will force recompiling sources
# make  TESTCASE=test_readback COCOTB_RESULTS_FILE=results_readback.xml | tee simlog_readback.log

# ii.
OSVVM_LOGLEVEL=DEBUG LOGGING=DEBUG TESTCASE=test_readback C_REGBANKLENGTH=600 TEST_READBACK_NB_TIMES=20 COCOTB_RESULTS_FILE=results_readback.xml make -f Makefile.ft2232h

