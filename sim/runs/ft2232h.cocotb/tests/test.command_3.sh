#!/bin/sh

export G_NB_ASICS=8		# always use 8 asics

# run command3 test

git checkout hdl/ft2232h_tb_pkg.vhd # reset regbanklength to its default value

# i.
# export DEBUG=1 # store trace
# make TESTCASE=test_command3 COCOTB_RESULTS_FILE=results_command3.xml | tee simlog_command3.log

# ii.
OSVVM_LOGLEVEL=DEBUG LOGGING=INFO TESTCASE=test_command3 COCOTB_RESULTS_FILE=results_command3.xml make -f Makefile.ft2232h
