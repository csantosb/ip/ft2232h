-- * P1 Counter

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

library osvvm;
context osvvm.OsvvmContext;

-- * Packages
use ieee.std_logic_1164.all;
use ieee.numeric_std_unsigned.all;
use work.ft2232h_instance_pkg.all;

-- * Entity

--! Entity simple description

--! This is an example peripheral implementing a simple up counter
--! It does
--! and ...

entity p1_counter is
  port (
    clk          : in    std_logic;           --! clock
    global_reset : in    std_logic;           --! global reset
    p_if         : inout rc_peripheral_if_p1  -- peripheral p1
    );
end entity p1_counter;

-- * Architecture

--! Architecture simple description

--! architecture : This is an example peripheral implementing a simple up counter
--! It does
--! and ...

architecture behavioral of p1_counter is

  -- ** Types

  --! @brief Architecture simple description

  --! architecture : This is an example peripheral implementing a simple up counter
  --! It does
  --! and ...

  type command1_state_type is (command1_state_1, command1_state_2);

  -- ** Signals

  --! Architecture simple description
  signal fifo1_clear        : std_logic                     := '0';
  signal fifo1_full         : std_logic                     := '0';
  signal fifo1_wrreq        : std_logic                     := '0';
  signal fifo1_datain       : std_logic_vector(31 downto 0) := (others => '0');
  signal fifo1_datain1      : std_logic_vector(7 downto 0)  := X"00";
  signal fifo1_datain2      : std_logic_vector(7 downto 0)  := X"01";
  signal fifo1_datain3      : std_logic_vector(7 downto 0)  := X"02";
  signal fifo1_datain4      : std_logic_vector(7 downto 0)  := X"03";
  signal fill_fifo1_counter : std_logic_vector(31 downto 0) := (others => '0');
  signal command1_state     : command1_state_type;

  constant p1_counter_id : alertlogidtype := GetAlertLogID("*p1 counter    .*");

begin

  -- ** Generic

  fifo1_clear <= not(p_if.enable);

  -- ** Write Fifo

  --! @brief process: description 1
  --! @brief process: description 2
  u_p : process (clk) is
  begin

    if (clk'event and clk = '1') then  --On met les données d'ecriture sur le bus de la fifo sur front
      if (global_reset = '1' or p_if.enable = '0') then
        fifo1_datain1 <= X"00";
        fifo1_datain2 <= X"01";
        fifo1_datain3 <= X"02";
        fifo1_datain4 <= X"03";

        fifo1_datain <= (others => '0');
        fifo1_wrreq  <= '0';

        fill_fifo1_counter <= (others => '0');
        command1_state     <= command1_state_1;
      else

        if (fifo1_wrreq) then
          -- logging
          Log(p1_counter_id, "  writes new word: " & to_hstring(fifo1_datain), DEBUG);
          -- logging
        end if;

        case command1_state is

          when command1_state_1 =>

            fifo1_wrreq <= '0';
            if (fill_fifo1_counter < p_if.params(0)) and
              (fifo1_full = '0') then
              command1_state <= command1_state_2;
            end if;

          when command1_state_2 =>

            if (fifo1_full = '0') then
              fifo1_datain <= fifo1_datain1 & fifo1_datain2
                              & fifo1_datain3 & fifo1_datain4;
              fifo1_datain1      <= fifo1_datain1 + 4;
              fifo1_datain2      <= fifo1_datain2 + 4;
              fifo1_datain3      <= fifo1_datain3 + 4;
              fifo1_datain4      <= fifo1_datain4 + 4;
              fill_fifo1_counter <= fill_fifo1_counter + 4;
              fifo1_wrreq        <= '1';
              command1_state     <= command1_state_1;
            else
              fifo1_wrreq    <= '0';
              command1_state <= command1_state_2;
              -- logging
              Log(p1_counter_id, "  fifo full ... !", DEBUG);
            -- logging
            end if;

        end case;

      end if;
    end if;

  end process u_p;

  -- ** Fifo

  u_Cati_output_fifo1 : entity work.cati_output_fifo1
    port map (
      rst    => fifo1_clear,
      wr_clk => clk,
      rd_clk => p_if.clk,
      din    => fifo1_datain,
      wr_en  => fifo1_wrreq,
      rd_en  => p_if.rd,
      dout   => p_if.data,
      full   => fifo1_full,
      empty  => p_if.empty
      );

end architecture behavioral;
